# 中文代码快速补全

[![installs](https://vsmarketplacebadge.apphb.com/installs/CodeInChinese.ChineseInputAssistant.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=CodeInChinese.ChineseInputAssistant)
[![version](https://vsmarketplacebadge.apphb.com/version/CodeInChinese.ChineseInputAssistant.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=CodeInChinese.ChineseInputAssistant)
[![中文编程](https://gitee.com/Program-in-Chinese/overview/raw/master/%E4%B8%AD%E6%96%87%E7%BC%96%E7%A8%8B.png)](https://gitee.com/Program-in-Chinese/overview)

![](截图/安装方法.gif)

## 功能简介

### 英文输入下，匹配中文标识符

不限于当前文件内标识符的补全（需先安装提供编程语言对应自动补全的插件），如导入库的方法补全：

![演示](截图/演示_库.png)

支持多种中文输入方式：

![演示](截图/输入法选项.png)

各种编程语言在全拼下已实测过的：

- JavaScript

![演示](截图/演示_JS.png)

- PHP

对 $ 开头的变量名，直接输入拼音即有提示：

![演示](截图/演示_PHP.png)

- Python

![演示](截图/演示_Python.png)

- TypeScript

![演示](截图/演示_TS.png)

当无编程语言支持插件时，也支持当前文件中的中文字段补全：

![演示](截图/演示_Kotlin.png)

同样支持中英混合命名：

![演示](截图/演示_中英混合.png)

### 第三方 API 集成

试添加了谷歌（需可访问 inputtools.google.com ）和百度的拼音 API：

![选项](截图/选项_输入法API_.png)

输入新标识符时也不用切换：

![演示](截图/演示_输入法API.png)

### 多音字

如存在多音字，可以匹配任意拼音组合：

![演示](截图/演示_多音字.png)

### 中文 snippet

现有 2 个 Python、1 个 JavaScript 片段。如有相关建议如使用问题、希望添加的片段，欢迎[提 issue](https://gitee.com/Program-in-Chinese/vscode_Chinese_Input_Assistant/issues)。支持中、英、拼音三种触发方式，下面是拼音演示（其他方式与瑕疵详见[此 PR](https://github.com/program-in-chinese/vscode_Chinese_Input_Assistant/pull/24)）：

![](截图/演示_snippet_拼音.png)

## [版本更新说明](CHANGELOG.md)

## [已知问题](文档/Issues.md)

## 其他平台类似支持

[IntelliJ IDEA/WebStorm/PyCharm...](https://gitee.com/tuchg/ChinesePinyin-CodeCompletionHelper)
