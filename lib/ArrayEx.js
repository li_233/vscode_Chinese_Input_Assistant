exports.多重笛卡尔积 = arr => arr.reduce((as, bs) => as.map(a => bs.map(b => [...a, b])).flat(), [[]])
exports.数组去重 = (判别式, 数组) => {
    var 包含 = (判别式, 项, 数组) => {
        for (var i = 0; i < 数组.length; i++) {
            if (判别式(数组[i], 项))
                return true
        }
        return false
    }

    var r = []
    for (var i = 0; i < 数组.length; i++) {
        if (!包含(判别式, 数组[i], r))
            r.push(数组[i])
    }
    return r
}
